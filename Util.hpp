#ifndef __MY_UTIL__
#define __MY_UTIL__
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <experimental/filesystem>
#include <sys/stat.h>
#include <jsoncpp/json/json.h>
#include "bundle.h"

namespace cloud{
	namespace fs = std::experimental::filesystem;
    //文件使用工具类
	class FileUtil
    {
		private:
			std::string _filename;//文件名称
		public:
			FileUtil(const std::string &filename)
                :_filename(filename)
            {}

			bool Remove()
            {
				if (this->Exists()== false) {
					return true;
				}
				remove(_filename.c_str());
				return true;
			}

            //获取文件大小
			int64_t FileSize()
            {
				struct stat st;
                //int stat(const char* path, struct stat* buf)获取文件信息并且放到buf中
				if (stat(_filename.c_str(), &st) < 0) {
					std::cout << "get file size failed!\n";
					return -1;
				}
				return st.st_size;
			}

            //文件最后修改 时间
			time_t LastMTime()
            {
				struct stat st;
                
				if (stat(_filename.c_str(), &st) < 0) 
                {
					std::cout << "get file size failed!\n";
					return -1;
				}
				return st.st_mtime;
			}

            //文件最后访问时间
			time_t LastATime() 
            {
				struct stat st;
				if (stat(_filename.c_str(), &st) < 0) {
					std::cout << "get file size failed!\n";
					return -1;
				}
				return st.st_atime;
			}

            //获取文件名称
			std::string FileName()
            {
                //以'/'为间隔，只要找到最后一个斜杠，那么其后面的就是文件名
				// ./abc/test.txt
				size_t pos = _filename.find_last_of("/");
				if (pos == std::string::npos) {
					return _filename;
				}
				return _filename.substr(pos+1);
			}


            //获取文件指定位置，指定长度的数据
			bool GetPosLen(std::string *body, size_t pos, size_t len)
            {
                //先获取文件大小，看看pos和len是否合法
				size_t fsize = this->FileSize();
				if (pos + len > fsize){
					std::cout << "get file len is error\n";
					return false;
				}
				std::ifstream ifs;
				ifs.open(_filename, std::ios::binary);
				if (ifs.is_open() == false) {
					std::cout << "read open file failed!\n";
					return false;
				}
                //从文件起始位置跳到pos处
				ifs.seekg(pos, std::ios::beg);
                //调整为len大小
				body->resize(len);
                //读取数据到body
				ifs.read(&(*body)[0], len);
                //判断上述操作是否正常
				if (ifs.good() == false) {
					std::cout << "get file content failed\n";
					ifs.close();
					return false;
				}
				ifs.close();
				return true;
			}

            //获取文件内容，也就是从0开始，获取文件长度的数据
			bool GetContent(std::string *body) 
            {
				size_t fsize = this->FileSize();
				return GetPosLen(body, 0, fsize);
			}

            //写入文件内容
			bool SetContent(const std::string &body) 
            {
				std::ofstream ofs;
				ofs.open(_filename, std::ios::binary);
				if (ofs.is_open() == false) {
					std::cout << "write open file failed!\n";
					return false;
				}
				ofs.write(&body[0], body.size());
				if (ofs.good() == false) {
					std::cout << "write file content failed!\n";
					ofs.close();
					return false;
				}
				ofs.close();
				return true;
			}


            //进行文件压缩
			bool Compress(const std::string &packname)
            {
				//1. 获取源文件数据
				std::string body;
				if (this->GetContent(&body) == false){
					std::cout << "compress get file content failed!\n";
					return false;
				}
				//2. 对数据进行压缩
				std::string packed = bundle::pack(bundle::LZIP, body);
				//3. 将压缩的数据存储到压缩包文件中
				FileUtil fu(packname);
				if (fu.SetContent(packed) == false){
					std::cout << "compress write packed data failed!\n";
					return false;
				}
				return true;
			}

            //进行压缩文件的解压
			bool UnCompress(const std::string &filename) 
            {
				//将当前压缩包数据读取出来
				std::string body;
				if (this->GetContent(&body) == false){
					std::cout << "uncompress get file content failed!\n";
					return false;
				}
				//对压缩的数据进行解压缩
				std::string unpacked = bundle::unpack(body);
				//将解压缩的数据写入到新文件
				FileUtil fu(filename);
				if (fu.SetContent(unpacked) == false)
				{
					std::cout << "uncompress write packed data failed!\n";
					return false;
				}
				return true;
			}

            //判断文件是否存在
			bool Exists() 
            {
				return fs::exists(_filename);
			}

            //创建一个文件夹
			bool CreateDirectory() 
            {
				if (this->Exists()) return true;
				return fs::create_directories(_filename);
			}

            //扫描文件夹
			bool ScanDirectory(std::vector<std::string> *arry) 
            {
				for(auto& p: fs::directory_iterator(_filename)) 
                {
					if (fs::is_directory(p) == true)
                    {
						continue;
					}
					//relative_path 带有路径的文件名
					//通过path找到绝对路径，通过relative_path找到相对路径，转换为string
					arry->push_back(fs::path(p).relative_path().string());
				}
				return true;
			}
	};

	//序列化与反序列化的封装
	class JsonUtil{
		public:
		/*/ 定义一个静态方法Serialize，用于将Json::Value对象序列化为字符串
     		参数：root - 需要序列化的Json::Value对象
           	str - 序列化后的字符串
    		返回值：布尔值，表示序列化是否成功*/
			static bool Serialize(const Json::Value &root, std::string *str)
			{
				Json::StreamWriterBuilder swb;
				std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
				std::stringstream ss; 
				if (sw->write(root, &ss) != 0)
				{
					std::cout << "json write failed!\n";
					return false;
				}
				*str = ss.str();
				return true;
			}

			/*/ 定义一个静态方法UnSerialize，用于将字符串反序列化为Json::Value对象
    		 	参数：str - 需要反序列化的字符串
    	       	root - 反序列化后的Json::Value对象
    			返回值：布尔值，表示反序列化是否成功*/
			static bool UnSerialize(const std::string &str, Json::Value *root)
			{
				Json::CharReaderBuilder crb;
				std::unique_ptr<Json::CharReader> cr(crb.newCharReader());
				std::string err;
				bool ret = cr->parse(str.c_str(), str.c_str() + str.size(), root, &err);
				if (ret == false) 
				{
					std::cout << "parse error: " << err << std::endl;
					return false; 
				}
				return true;
			}
	};
}
#endif
