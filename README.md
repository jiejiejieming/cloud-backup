# 云备份

#### 介绍
使用C++实现的云备份项目，其功能是客户端自动将本地计算机上指定文件夹中需要备份的文件上传备份到服务器中。并且能够随时通过浏览器进行查看并且下载，其中下载过程支持断点续传功能，而服务器也会对上传文件进行热点管理，将非热点文件进行压缩存储，节省磁盘空间。

#### 软件架构
实现两端程序，将服务器部署在Centos-7.6版本的Linux上，客户端部署在windows10上。
需要将Linux环境升级，也就是搭建-gcc升级7.3版本：

```
sudo yum install centos-release-scl-rh centos-release-scl
sudo yum install devtoolset-7-gcc devtoolset-7-gcc-c++
source /opt/rh/devtoolset-7/enable
echo "source /opt/rh/devtoolset-7/enable" >> ~/.bashrc
```


#### 安装教程

1.  bundle库
需要先安装git，接着从GitHub上下载：

```
sudo yum install git
git clone https://github.com/r-lyeh-archived/bundle.git
```


2.  httplib库

```
git clone https://github.com/yhirose/cpp-httplib.git
```


3.  jsoncpp库

```
sudo yum install epel-release
sudo yum install jsoncpp-devel
[wjmhlh@VM-12-9-centos ~]$ ls /usr/include/jsoncpp/json/
assertions.h config.h forwards.h reader.h version.h
autolink.h features.h json.h value.h writer.h
#注意，centos版本不同有可能安装的jsoncpp版本不同，安装的头文件位置也就可能不同了
```


#### 模块功能

实现服务器和客户端两端程序。

客户端程序主要的功能是

①自动检测指定文件夹中的文件，就是获取文件夹中有什么文件，

②获取后，判断是否需要备份，将需要备份的文件逐个上传到服务器上，但是已经备份过了但是又修改，那么处理的方法是上传后又修改，但是已经间隔了1分钟都没修改，那么就进行备份。

客户端模块：

①数据管理模块：保存备份的文件信息，用于在文件检测，判断（判断是否存在，是否相等）是否需要备份。

②文件检测模块：监控指定的文件夹。

③文件备份模块：上传需要备份的文件数据。

服务端程序主要的功能是针对客户端上传的文件进行备份存储，对文件进行热点文件管理，非热点文件进行压缩存储，支持客户端浏览器查看文件列表以及下载文件，支持断点续传。

服务端模块：

①数据管理模块：管理备份的文件信息，便于随时获取信息

②网络通信模块：实现与客户端的网络通信。

③业务处理模块：上传、列表、下载、断点续传

④热点管理模块：对长时间无访问的文件进行压缩存储。


